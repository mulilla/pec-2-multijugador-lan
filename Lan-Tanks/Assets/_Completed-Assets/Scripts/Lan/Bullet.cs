using UnityEngine;

public class Bullet : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {        
        GameObject hit = col.gameObject;
        Health health = hit.GetComponent<Health>();
        if (health != null)
        {
            health.TakeDamage(20);
        }

        Destroy(gameObject);
    }
}
